require "application_system_test_case"

class BibliotecasTest < ApplicationSystemTestCase
  setup do
    @biblioteca = bibliotecas(:one)
  end

  test "visiting the index" do
    visit bibliotecas_url
    assert_selector "h1", text: "Bibliotecas"
  end

  test "creating a Biblioteca" do
    visit bibliotecas_url
    click_on "New Biblioteca"

    fill_in "Endereco", with: @biblioteca.endereco
    fill_in "Nome", with: @biblioteca.nome
    click_on "Create Biblioteca"

    assert_text "Biblioteca was successfully created"
    click_on "Back"
  end

  test "updating a Biblioteca" do
    visit bibliotecas_url
    click_on "Edit", match: :first

    fill_in "Endereco", with: @biblioteca.endereco
    fill_in "Nome", with: @biblioteca.nome
    click_on "Update Biblioteca"

    assert_text "Biblioteca was successfully updated"
    click_on "Back"
  end

  test "destroying a Biblioteca" do
    visit bibliotecas_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Biblioteca was successfully destroyed"
  end
end
