json.extract! livro, :id, :nome, :numero_identificacao, :quantidade, :created_at, :updated_at
json.url livro_url(livro, format: :json)
