json.extract! biblioteca, :id, :nome, :endereco, :created_at, :updated_at
json.url biblioteca_url(biblioteca, format: :json)
