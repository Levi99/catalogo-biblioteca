class AddBibliotecaToLivro < ActiveRecord::Migration[5.2]
  def change
    add_reference :livros, :biblioteca, foreign_key: true
  end
end
