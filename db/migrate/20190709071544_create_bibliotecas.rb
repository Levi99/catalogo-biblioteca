class CreateBibliotecas < ActiveRecord::Migration[5.2]
  def change
    create_table :bibliotecas do |t|
      t.string :nome
      t.string :endereco

      t.timestamps
    end
  end
end
