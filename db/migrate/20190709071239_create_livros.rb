class CreateLivros < ActiveRecord::Migration[5.2]
  def change
    create_table :livros do |t|
      t.string :nome
      t.integer :numero_identificacao
      t.integer :quantidade

      t.timestamps
    end
  end
end
